import './App.css';
import AppNavbar from './components/AppNavbar.js';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext.js';
import Home from './pages/Home.js';
import NotFound from './pages/NotFound.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Profile from './pages/Profile.js';
import Products from './pages/Products.js';
import ProductItem from './pages/ProductItem.js';
import AddProduct from './pages/AddProduct.js';
import Footer from './components/Footer.js';
import Users from './pages/Clients.js';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/detalis`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      id: localStorage.getItem('userId')
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result._id !== 'undefined'){
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      } else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <>
    <div className="app">
          <UserProvider value={{user, setUser, unsetUser}}>
              <Router>
              <AppNavbar/>
                <Container>
                  <Routes>
                    <Route path='/' element={<Home/>}/>
                    <Route path='/Register' element={<Register/>}/>
                    <Route path='/Login' element={<Login/>}/>
                    <Route path='/Logout' element={<Logout/>}/>
                    <Route path='/Profile' element={<Profile/>}/>
                    <Route path='/Products' element={<Products/>}/>
                    <Route path='/Products/:productId' element={<ProductItem/>}/>
                    <Route path='/Products/add' element={<AddProduct/>}/>
                    <Route path='/Users' element={<Users/>}/>
                    <Route path='*' element={<NotFound/>}/>
                  </Routes>
                </Container>
                <Footer/>
              </Router>
            </UserProvider>
    </div>
        
    </>
  );
}

export default App;