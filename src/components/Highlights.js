import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row className="my-3 text-center">
      <Col xs={12} md={4} className="d-flex mb-4" data-aos="fade-up" data-aos-duration="500">
        <Card className="cardHighlight p-3 flex-fill hvr-grow h-100 border-0 shadow">
          <Card.Body className="d-flex flex-column palette2">
            <Card.Img className="rounded-circle mx-auto" style={{ maxWidth: '100%', height: 'auto' }} variant="top" src="https://s26162.pcdn.co/wp-content/uploads/2021/02/japanese-food.jpg" />
            <Card.Title>
              <h2 className="mb-0">Taste of Japan</h2>
            </Card.Title>
            <Card.Text className="flex-grow-1">
              Delight your taste buds with an array of authentic Japanese treats, from fresh bento boxes to sweet wagashi and unique beverages.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="d-flex mb-4" data-aos="fade-up" data-aos-duration="1000">
        <Card className="cardHighlight p-3 flex-fill hvr-grow h-100 border-0 shadow">
          <Card.Body className="d-flex flex-column palette2">
            <Card.Img className="rounded-circle mx-auto" style={{ maxWidth: '100%', height: 'auto' }} variant="top" src="https://rimage.gnst.jp/livejapan.com/public/article/detail/a/00/01/a0001026/img/basic/a0001026_main.jpg?20190104144113" />
            <Card.Title>
              <h2 className="mb-0">Everyday Essentials</h2>
            </Card.Title>
            <Card.Text className="flex-grow-1">
              Stay prepared with practical essentials, from toiletries to travel gear, ensuring you're ready for any adventure.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="d-flex mb-4" data-aos="fade-up" data-aos-duration="1500">
        <Card className="cardHighlight p-3 flex-fill hvr-grow h-100 border-0 shadow">
          <Card.Body className="d-flex flex-column palette2">
            <Card.Img className="rounded-circle mx-auto" style={{ maxWidth: '100%', height: 'auto' }} variant="top" src="https://upload.wikimedia.org/wikipedia/commons/0/0d/Great_Wave_off_Kanagawa2.jpg" />
            <Card.Title>
              <h2 className="mb-0">Cultural Treasures</h2>
            </Card.Title>
            <Card.Text className="flex-grow-1">
              Immerse yourself in Japanese culture through traditional ceramics, artful souvenirs, and captivating decorative items. Tokyo Konbini: Where convenience meets culture in every aisle.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
