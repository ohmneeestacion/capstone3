import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AOS from 'aos';
import 'aos/dist/aos.css'; 
AOS.init();

export default function Banner() {
  return (
    <Row data-aos="fade-up" data-aos-duration="1000" className="rounded banner-container">
      <Col className="banner-content p-5 col-5 shadow" >
        <h2 data-aos="fade-up" className="mb-2 hvr-grow">Welcome to Tokyo Konbini</h2>
         
       <Link data-aos="fade-up" className="btn btn-primary hvr-grow btn-lg mt-3" to="/products">
          Shop Now
        </Link>
      </Col>

      <div 
        className="banner-image rounded"
        style={{
          backgroundImage:
            "url('https://i.ibb.co/k6p1CHg/Banner.png')",
        }}
      ></div>

    </Row>

  );
}