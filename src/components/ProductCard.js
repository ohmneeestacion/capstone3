import {Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({product}){

	// Destructuring the contents of 'product'
	const {_id, name, description, price, image} = product;

	// A state is just like a variable but with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state.



		if (product.isActive === false) {
			return (
				<Card className="p-3 m-2 col-4 product-card" style={{ width: '24rem' }} data-aos="fade-up" data-aos-duration="1000">
			      <Card.Body>
			        <Card.Title className="product-title">{name}</Card.Title>
			        <Card.Img className="product-image mb-2 hvr-grow" variant="top" src={image} alt={name} />
			        <Card.Subtitle className="product-description">Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			      </Card.Body>
			      <Card.Footer>
			        <Card.Subtitle className="product-price">Price:</Card.Subtitle>
			        <Card.Text>PHP {price}</Card.Text>
			        <Link className="btn btn-secondary align-bottom">
			          ****OUT OF STOCK****
			        </Link>
			      </Card.Footer>
			    </Card>
			)
		} else {
			return (
				<Card className="p-3 m-2 col-4 product-card" style={{ width: '24rem' }} data-aos="fade-up" data-aos-duration="1000">
			      <Card.Body>
			        <Card.Title className="product-title">{name}</Card.Title>
			        <Card.Img className="product-image mb-2 hvr-grow" variant="top" src={image} alt={name} />
			        <Card.Subtitle className="product-description">Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			      </Card.Body>
			      <Card.Footer>
			        <Card.Subtitle className="product-price">Price:</Card.Subtitle>
			        <Card.Text>PHP {price}</Card.Text>
			        <Link className="btn btn-primary align-bottom" to={`/products/${_id}`}>
			          View Info
			        </Link>
			      </Card.Footer>
			    </Card>
			)
		}
		
		
}

// PropTypes is used for validating the data from the props
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}