import {Button, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditUser({user_id, fetchUsers}){

  const [isAdmin, setIsAdmin] = useState(false);

  const editUser = (event, userId) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/api/users/${user_id}/modifyrole`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isAdmin: isAdmin
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result){
        Swal.fire({
          title: 'User has Admin Access!',
          text: 'Role Updated!',
          icon: 'success'
        })

        // Triggers the fetching of all Users after updating a User. The 'fetchUsers' function comes from the props that were passed from the Users.js component to the AdminView.js
        fetchUsers();
      } else {
        Swal.fire({
          title: 'Something went wrong',
          text: 'Please try again.',
          icon: 'error'
        })
        fetchUsers();
      }
    })
  }

  return (
    <>
          <Form className="text-center" onSubmit={event => editUser(event, user_id)}>
            <Form.Group controlId="userIsAdmin">
              <Form.Check 
                type="checkbox" 
                checked={isAdmin} 
                onChange={event => setIsAdmin(event.target.checked)}  
                label="Change Role" 
              />
            </Form.Group>
            <Button  variant='success' type='submit'>Submit</Button>
            </Form>
    </>
  );

}