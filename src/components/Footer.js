import React from 'react';

const Footer = () => {
  return (
    <footer className="palette1 py-2 footer">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <h4>About Us</h4>
            <p>Discover Tokyo Konbini: Where modern convenience meets timeless charm. Immerse yourself in a curated fusion of Japanese tradition and essentials, from delectable bento to everyday needs. Our friendly team embodies authentic hospitality, bridging cultures and making convenience an experience.</p>
          </div>
          <div className="col-md-4 my-2 text-center">
            <h4>Follow Us</h4>
            <div className="social-icons">
              <a href="https://www.facebook.com/" className="hvr-grow text-dark me-3" target="_blank" rel="noreferrer">
                <img width="48" src="https://upload.wikimedia.org/wikipedia/en/thumb/0/04/Facebook_f_logo_%282021%29.svg/800px-Facebook_f_logo_%282021%29.svg.png" alt="Facebook"/>
              </a>
              <a href="https://twitter.com/" className="hvr-grow text-dark me-3" target="_blank" rel="noreferrer">
                <img width="48" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/X_logo_2023.svg/1200px-X_logo_2023.svg.png" alt="Twitter"/>
              </a>
              <a href="https://www.instagram.com/" className="hvr-grow text-dark me-3" target="_blank" rel="noreferrer"> 
              <img width="48" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Instagram_logo_2022.svg/1024px-Instagram_logo_2022.svg.png" alt="instagram"/> 
              </a>
            </div>
          </div>
          <div className="col-md-4 text-center">
            <h4>Contact Us</h4>
            <p>Feel free to get in touch with us.</p>
            <form className="contact-form">
                <div className="mb-3">
                    <textarea className="form-control" placeholder="Message"></textarea>
                </div>
                <div className="d-flex justify-content-end">
                    <button type="submit" className="btn btn-primary">Send</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
