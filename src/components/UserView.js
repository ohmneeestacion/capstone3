import {useState, useEffect} from 'react';
import {Col, Row} from 'react-bootstrap';
import ProductCard from './ProductCard.js';

export default function UserView({productsData}){
	const [products, setProducts] = useState([])

	useEffect(() => {
		// Filters out the products to only be products that are active
		const active_products = productsData.map(product => {
				return (
					<ProductCard product={product} key={product._id}/>
				)
			
		})

		// Set the products state to the active products array
		setProducts(active_products);
	}, [productsData])

	return(
		<>
				
			<Col className="container text-center">
      		<Row className="justify-content-center h-100">
      		<h1 className="p-2">Products</h1>
				{ products }
			</Row>
			</Col>
		</>
	)
}
