import {Table} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';


export default function AdminView({productsData, fetchProducts, usersData, fetchUsers}){
	const [products, setProducts] = useState([])

	useEffect(() => {
		const products_array = productsData.map(product => {
			return (
				<tr key={product._id}>
					<td><img width="75" src={product.image} alt="" className="hvr-grow"/></td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.isActive ? 'Available': 'Unavailable'}</td>
					<td>
						<EditProduct product_id={product._id} fetchProducts={fetchProducts}/>
					</td>
					<td>
						<ArchiveProduct product_id={product._id} fetchProducts={fetchProducts} isActive={product.isActive}/>
					</td>			
				</tr>	

			)
		})

		setProducts(products_array);
	}, [productsData, fetchProducts])



	return (
		<>
			<h1 className="text-center py-3">Admin Dashboad</h1>
			<div className="p-3 mb-5 bg-light">
			<Table striped bordered hover responsive>
				<thead>
				<tr>
					<th>Image</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Availability</th>
					<th>Edit</th>
					<th>Actions</th>
				</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
			</div>
		</>
	)
}