export default function NotFound(){
	return (
		<>
		<h1 className="mt-5 text-center">Not Found</h1>,
		<p className="my-3 text-center">The page you visited could not be found</p>,
		</>
	)
}