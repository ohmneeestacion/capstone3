import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext.js';
import EditUser from '../components/EditUser.js';
import NotFound from './NotFound.js';


export default function Users(){
	const {user} = useContext(UserContext);

	const [users, setUsers] = useState([]);

	const fetchUsers = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/allusers`)
		.then(response => response.json())
		.then(result => {
			setUsers(result)
		})
	}
	// The useEffect hook will run everytime the Courses page loads, which will then retrieve all courses from the API and set it to their specific CourseCards.
	useEffect(() => {
		fetchUsers()
	}, [])

	return (
		<>
		{
			(user.isAdmin === true) ?
    <div>
      <h1 className="text-center py-3">List of Clients</h1>
      <div className="p-3 mb-5 bg-light">
        <div className="table-responsive">
          <table className="table table-striped table-bordered table-hover">
            <thead>
              <tr className="text-center">
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
                
              </tr>
            </thead>
            <tbody>
              {users.map(user => (
                <tr key={user._id}>
                  <td>{user._id}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td>{user.email}</td>
                  <td>{user.isAdmin ? 'Admin' : 'User'}</td>
                  <td><EditUser user_id={user._id} fetchUsers={fetchUsers}/></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
			:
				<NotFound/>
			
		}
		</>
  );
}