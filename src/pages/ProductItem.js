import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductItem(){
	// The use Params hook will allow us to access the product ID
	const {productId} = useParams();

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);

	const AddToCart = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/addtocart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: id
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);
			if(result.message === 'Add to Cart successfully!'){
				Swal.fire({
					title: "Item added to cart",
					text: result.message,
					icon: 'success'
				})

				navigate('/products')
			} else {
				Swal.fire({
					title: "Something went wrong",
					text: 'Please try again',
					icon: 'error'
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setImage(result.image)
		})
	}, [productId])

	return (
		<Container className="my-5">
			<Row>
				<Col>
					<Card>
						<Card.Body className="text-center w-25 mx-auto">
							<Card.Img height="200" className="my-2 hvr-grow shadow" variant="top" src={image}/>
							<Card.Title><h1>{name}</h1></Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>PHP {price}</Card.Text>

							{ (user.id !== null) ?
							<>
								<Button variant="primary" onClick={()=> AddToCart(productId)}>Add to Cart</Button>
								<p><Link className="btn my-1 btn-secondary btn-block" to ='/products'>Back to Products</Link></p>
							</>
							:
								<Link className="btn btn-secondary btn-block" to ='/login'>Need to Login first.</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}