import { useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';
import UserContext from '../UserContext.js';

export default function Products(){
	const {user} = useContext(UserContext);

	const [products, setProducts] = useState([]);

	const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/all`)
		.then(response => response.json())
		.then(result => {
			setProducts(result)
		})
	}
	// The useEffect hook will run everytime the Courses page loads, which will then retrieve all courses from the API and set it to their specific CourseCards.
	useEffect(() => {
		fetchProducts()
	}, [])

	return(
		<>
		{
			(user.isAdmin === true) ?
				<AdminView productsData={products} fetchProducts={fetchProducts}/>
			:
				<UserView productsData={products}/>
			
		}
		</>
	)
}
