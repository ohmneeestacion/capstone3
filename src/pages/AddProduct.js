import {useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddProduct(){
	// Initialization
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	// States
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [image, setImage] = useState("")

	// Functions
	function createProduct(event){
		// Prevents the default behavior of page reload when submitting a form
		event.preventDefault();

		let token = localStorage.getItem('token');

		// Fetch function to the product creation API
		fetch(`${process.env.REACT_APP_API_URL}/api/products/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'New Product Added',
					icon: 'success'
				})

				// For clearing the form
				setName('')
				setDescription('')
				setPrice(0)
				setImage('')

				// Redirect to /products after creation of a new product
				navigate('/products')
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Product creation unsuccessful',
					icon: 'error'
				})
			}
		})
	}

	return (
		(user.isAdmin === true) ? 
			<>
				<h1 className="my-3 text-center">Add Product</h1>
                <Form className="w-50 mx-auto" onSubmit={event => createProduct(event)}>
                    <Form.Group>
                        <Form.Label>Product Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={event => {setName(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
					    <Form.Label>Product Description:</Form.Label>
					    <textarea
					        className="form-control"
					        placeholder="Enter Description"
					        required
					        rows={7}
					        value={description}
					        onChange={event => {
					            setDescription(event.target.value);
					        }}
					    ></textarea>
					</Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Product Image:</Form.Label>
                        <Form.Control type="text" placeholder="Insert Image URL" required value={image} onChange={event => {setImage(event.target.value)}}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" className="my-5">Submit</Button>
                </Form>
			</>
		:
			<Navigate to='/products'/>
	)
}
