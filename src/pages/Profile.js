import React from 'react';
import { Row, Col, Modal, Button, Form } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {Navigate} from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function Profile(){
  const {user} = useContext(UserContext);
  const [details, setDetails] = useState({})
  const [productCounts, setProductCounts] = useState({});
  const [productData, setProductData] = useState([]);
  const [show, setShow] = useState(false);
  
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleOrder = () => {
    Swal.fire({
        title: 'Order Placed Successfully',
        icon: 'success'
    })
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: user.id
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result._id !== 'undefined'){
        setDetails(result)
      }
    })
  }, [user.id])

  useEffect(() => {
    if (details.orderedProducts) {
      const orderedProductsArray = Object.values(details.orderedProducts);

      const productCounts = {};

      for (const item of orderedProductsArray) {
        if (!productCounts[item.productId]) {
          productCounts[item.productId] = 1;
        } else {
          productCounts[item.productId]++;
        }
      }

      setProductCounts(productCounts);
    }
  }, [details.orderedProducts]);

  useEffect(() => {
    const fetchUniqueProductData = async () => {
      const uniqueProductIds = Object.keys(productCounts);

      const fetchedProductData = [];

      for (const productId of uniqueProductIds) {
        try {
          const response = await fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`);
          const productDetails = await response.json();
          fetchedProductData.push({ ...productDetails, count: productCounts[productId] });
        } catch (error) {
          console.error(`Error fetching data for productId ${productId}:`, error);
        }
      }

      setProductData(fetchedProductData);
    };

    fetchUniqueProductData();
  }, [productCounts]);

  const calculateTotalSum = products =>
    products.reduce((total, product) => total + product.count * product.price, 0);

  return (
    (user.id === null) ?
      <Navigate to='*'/>
    :
      <div className="p-5 text-center">
        <Row>
          <Col md={6}>
            <h2 className="my-4">Profile</h2>
            <img data-aos="zoom-in" data-aos-duration="1000" data-aos-anchor-placement="bottom-bottom" className="rounded-circle my-3 shadow hvr-grow" width="250" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOHsI8_ls6q1h5ySN3Hqyr-wj8bHvPdzx3dTiosDK9QcGzSj9Ae7Bm_ylZXgssilHqlwE&usqp=CAU" alt=""/>
            <h1>{`${details.firstName} ${details.lastName}`}</h1>
            <h4 className="my-2">Contact Information</h4>
            <p>Email: {details.email} </p>
            <p>Mobile Number: {details.mobileNo} </p>
          </Col>
          <Col md={6}>
            <h2 className="my-2 p-3 palette5 rounded ">Cart Details</h2>
            <p>
              <h3 className="p-2 palette2 rounded">Total Amount: {calculateTotalSum(productData)} Pesos</h3>
            </p>
                  <>
                    <Button className="mb-3" variant="primary" onClick={handleShow}>
                      Check Out
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>Enter Shipping Address</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <Form>
                          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Block/Lot #</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Block 123/Lot 456"
                              autoFocus
                            />
                          </Form.Group>
                          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Streetname</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Bugbugan Street"
                              autoFocus
                            />
                          </Form.Group>
                          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Barangay/Subdivision</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Barangay Tahimik"
                              autoFocus
                            />
                          </Form.Group>
                          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Zip Code</Form.Label>
                            <Form.Control
                              type="number"
                              placeholder="0420"
                              autoFocus
                            />
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlTextarea1"
                          >
                            <Form.Label>Special Instructions/Request</Form.Label>
                            <Form.Control as="textarea" rows={3} />
                          </Form.Group>
                        </Form>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Cancel
                        </Button>
                        <Button variant="primary" onClick={() =>{ handleClose(); handleOrder();}}>
                          Place Order
                        </Button>
                      </Modal.Footer>
                    </Modal>
                   </>
  
            {productData.map(product => {
              if (product.isActive === false) {
                product.price = 0; // Set price to zero for inactive products
                
                return (
                  <div key={product._id} className="mb-4 product-item p-3 palette2 rounded shadow">
                    <Row>
                      <Col xs={12} sm={6} className="product-image">
                        <img className="img-fluid hvr-grow shadow rounded" src={product.image} alt={product.name} />
                      </Col>
                      <Col xs={12} sm={6} className="product-details">
                        <p className="product-name">
                          {product.name}
                        </p>
                        <p className="product-price">
                          ****OUT OF STOCK****
                        </p>
                      </Col>
                    </Row>
                  </div>
                );
              } else {
                return (
                  <div key={product._id} className="mb-4 product-item p-3 palette5 rounded shadow hvr-grow">
                    <Row>
                      <Col xs={12} sm={6} className="product-image">
                        <img className="img-fluid hvr-grow shadow rounded" src={product.image} alt={product.name} />
                      </Col>
                      <Col xs={12} sm={6} className="product-details">
                        <p className="product-name">
                          {product.name}
                        </p>
                        <p className="product-price">
                          Price: PHP {product.price.toFixed(2)}
                        </p>
                        <p className="product-count">
                          Quantity: {product.count}
                        </p>
                        <p className="product-total">
                          Total: PHP {(product.count * product.price).toFixed(2)}
                        </p>
                      </Col>
                    </Row>
                  </div>
                );
              }
            })}
         </Col>
        </Row>
      </div>

  );
};
