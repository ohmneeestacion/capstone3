import { Form, Button} from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function LoginForm() {
  //
  const {user, setUser} = useContext(UserContext);

  // Create input states for the form
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Create a state for conditionally rendering the submit button
  const [isActive, setIsActive] = useState(true);

  // Use useEffect to enable/disable the submit button based on input states
  useEffect(() => {
		if((email !== "" && password !== "")) setIsActive(true);
		else setIsActive(false);
	}, [email, password]);

  // Handle form submit event
  const loginUser = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				setEmail("");
				setPassword("");
				console.log(result);
				if(result.accessToken) {
					localStorage.setItem('token', result.accessToken);
					localStorage.setItem('userId', result.userId);

					retrieveUserDetails(result.accessToken, result.userId)

					Swal.fire({
						title: 'Login Success',
						text: 'You have logged in successfully!',
						icon: 'success'
					})
				}
				else Swal.fire({
						title: 'Something went wrong',
						text: `${email} does not exist`,
						icon: 'warning'
					})
				
			}
			else Swal.fire({
						title: 'Error Occured, Please try Again',
						icon: 'error'
					})
		})
	}

	const retrieveUserDetails = (token, userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				id: userId
			})
		})
		.then(response => response.json())
		.then(result => {
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}

  return (
  		(user.id !== null) ?
  		<Navigate to='/products'/>
  		:
		<Form className="w-50 mb-5 mx-auto" onSubmit = {event => loginUser(event)}>
        	<h1 className="my-4 text-center">Login</h1>     
            <Form.Group className = "p-3 bg-light">
                <Form.Label>Email:</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter Email" 
	                value = {email}
	                onChange = {event => {setEmail(event.target.value)}}
	                required
                />
            </Form.Group>
            <Form.Group className = "p-3 bg-light">
                <Form.Label>Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Enter Password" 
	                value = {password}
	                onChange = {event => {setPassword(event.target.value)}}
	                required
                />
            </Form.Group>
            <div className="text-center my-3">
            	<Button variant="primary" type="submit" disabled={isActive === false}>Submit</Button>
            </div>
              
        </Form>
  );
}
